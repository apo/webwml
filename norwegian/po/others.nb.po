msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2014-05-01 16:20+0200\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/banners/index.tags:7
msgid "Download"
msgstr "Hent"

#: ../../english/banners/index.tags:11
msgid "Old banner ads"
msgstr "Gamle reklamebannere"

#: ../../english/devel/debian-installer/ports-status.defs:10
msgid "Working"
msgstr "Fungerer"

#: ../../english/devel/debian-installer/ports-status.defs:20
msgid "sarge"
msgstr "sarge"

#: ../../english/devel/debian-installer/ports-status.defs:30
msgid "sarge (broken)"
msgstr "sarge (ødelagt)"

#: ../../english/devel/debian-installer/ports-status.defs:40
msgid "Booting"
msgstr "Starter opp"

#: ../../english/devel/debian-installer/ports-status.defs:50
msgid "Building"
msgstr "Bygger"

#: ../../english/devel/debian-installer/ports-status.defs:56
msgid "Not yet"
msgstr "Ikke enda"

#: ../../english/devel/debian-installer/ports-status.defs:59
msgid "No kernel"
msgstr "Ingen kjerne"

#: ../../english/devel/debian-installer/ports-status.defs:62
msgid "No images"
msgstr "Ingen bilder"

#: ../../english/devel/debian-installer/ports-status.defs:65
msgid "<void id=\"d-i\" />Unknown"
msgstr "<void id=\"d-i\" />Ukjent"

#: ../../english/devel/debian-installer/ports-status.defs:68
msgid "Unavailable"
msgstr "Utilgjengelig"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Hjørnet for nye medlemmer"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Steg 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Steg 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Steg 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Steg 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Steg 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Steg 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Steg 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Sjekkliste for søkere"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
msgid "More information"
msgstr "Mer informasjon"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Telefon"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Faks"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Adresse"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Med&nbsp;\"Debian\""

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Uten&nbsp;\"Debian\""

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr ""

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Drevet av Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Drevet av Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Debian drevet]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (miniknapp)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "samme som over"

#: ../../english/misc/merchandise.def:8
msgid "Products"
msgstr "Produkter"

#: ../../english/misc/merchandise.def:11
msgid "T-shirts"
msgstr "T-skjorter"

#: ../../english/misc/merchandise.def:14
msgid "hats"
msgstr "hatter"

#: ../../english/misc/merchandise.def:17
msgid "stickers"
msgstr "klisterlapper"

#: ../../english/misc/merchandise.def:20
msgid "mugs"
msgstr "krus"

#: ../../english/misc/merchandise.def:23
msgid "other clothing"
msgstr "andre klær"

#: ../../english/misc/merchandise.def:26
msgid "polo shirts"
msgstr "poloskjorter"

#: ../../english/misc/merchandise.def:29
msgid "frisbees"
msgstr "frisbee"

#: ../../english/misc/merchandise.def:32
msgid "mouse pads"
msgstr "musmatter"

#: ../../english/misc/merchandise.def:35
msgid "badges"
msgstr "merker"

#: ../../english/misc/merchandise.def:38
msgid "basketball goals"
msgstr "basketballmål"

#: ../../english/misc/merchandise.def:42
msgid "earrings"
msgstr "øreringer"

#: ../../english/misc/merchandise.def:45
msgid "suitcases"
msgstr "dresser"

#: ../../english/misc/merchandise.def:48
msgid "umbrellas"
msgstr "paraplyer"

#: ../../english/misc/merchandise.def:51
msgid "pillowcases"
msgstr "putevar"

#: ../../english/misc/merchandise.def:54
msgid "keychains"
msgstr ""

#: ../../english/misc/merchandise.def:57
msgid "Swiss army knives"
msgstr ""

#: ../../english/misc/merchandise.def:60
msgid "USB-Sticks"
msgstr ""

#: ../../english/misc/merchandise.def:75
msgid "lanyards"
msgstr ""

#: ../../english/misc/merchandise.def:78
msgid "others"
msgstr ""

#: ../../english/misc/merchandise.def:98
msgid "Donates money to Debian"
msgstr ""

#: ../../english/misc/merchandise.def:102
msgid "Money is used to organize local free software events"
msgstr ""

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Hvor lenge har du brukt Debian?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Er du en Debian-utvilker?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "Hvilke områder i Debian er du involvert i?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Hva fikk deg interessert i å jobbe med Debian?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""
"Har du noen tips til kvinner som er interessert i å bli mer involvert med "
"Debian?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr "Er du involvert med noen andre kvinner i teknologigrupper? Hvilke?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Litt mer om deg ..."

#: ../../english/y2k/l10n.data:6
msgid "OK"
msgstr "OK"

#: ../../english/y2k/l10n.data:9
msgid "BAD"
msgstr "DÅRLIG"

#: ../../english/y2k/l10n.data:12
msgid "OK?"
msgstr "OK?"

#: ../../english/y2k/l10n.data:15
msgid "BAD?"
msgstr "DÅRLIG?"

#: ../../english/y2k/l10n.data:18
msgid "??"
msgstr "??"

#: ../../english/y2k/l10n.data:21
msgid "Unknown"
msgstr "Ukjent"

#: ../../english/y2k/l10n.data:24
msgid "ALL"
msgstr "ALT"

#: ../../english/y2k/l10n.data:27
msgid "Package"
msgstr "Pakke"

#: ../../english/y2k/l10n.data:30
msgid "Status"
msgstr "Status"

#: ../../english/y2k/l10n.data:33
msgid "Version"
msgstr "Versjon"

#: ../../english/y2k/l10n.data:36
msgid "URL"
msgstr "Nettadresse"

#~ msgid "p<get-var page />"
#~ msgstr "s.<get-var page />"

#~ msgid "developers only"
#~ msgstr "kun for utviklere"

#~ msgid "deity developers only"
#~ msgstr "kun for deity-utviklere"

#~ msgid "Debian Technical Committee only"
#~ msgstr "Kun for Debians tekniske komité"

#~ msgid "closed"
#~ msgstr "lukket"

#~ msgid "open"
#~ msgstr "åpen"

#~ msgid "Unsubscribe"
#~ msgstr "Si opp"

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr "Velg listene du vil si opp abonnementet på:"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription "
#~ "web form</a> is also available, for subscribing to mailing lists. "
#~ msgstr ""
#~ "Se <a href=\"./#subunsub\">postlistesiden</a> for informasjon om hvordan "
#~ "du sier opp via e-post. En <a href=\"unsubscribe\">abonnementside</a> er "
#~ "også tilgjengelig, for å abonnere på postlister. "

#~ msgid "Mailing List Unsubscription"
#~ msgstr "Oppsigelse av postliste-abonnement"

#~ msgid ""
#~ "Please respect the <a href=\"./#ads\">Debian mailing list advertising "
#~ "policy</a>."
#~ msgstr ""
#~ "Vennligst respektér <a href=\"./#ads\">Debians retningslinjer for "
#~ "postlisteannonsering</a>."

#~ msgid "Clear"
#~ msgstr "Nullstill"

#~ msgid "Subscribe"
#~ msgstr "Abonnér"

#~ msgid "Your E-Mail address:"
#~ msgstr "E-postadressen din:"

#~ msgid "is a read-only, digestified version."
#~ msgstr "er en oppsummert versjon som bare kan leses."

#~ msgid "Subscription:"
#~ msgstr "Abonnement:"

# Sjekkes!
#~ msgid ""
#~ "Only messages signed by a Debian developer will be accepted by this list."
#~ msgstr "Listen godtar kun meldinger signert av Debianutviklere."

# Sjekkes!
#~ msgid "Posting messages allowed only to subscribers."
#~ msgstr "Kun medlemmer kan sende meldinger."

#~ msgid "Moderated:"
#~ msgstr "Ordstyrt:"

#~ msgid "No description given"
#~ msgstr "Beskrivelse savnes"

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Velg listene du vil abonnere på"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription "
#~ "web form</a> is also available, for unsubscribing from mailing lists. "
#~ msgstr ""
#~ "Se <a href=\"./#subunsub\">postlistesiden</a> for informasjon om hvordan "
#~ "du abonnerer via e-post. En <a href=\"unsubscribe\">oppsigelsesnettside</"
#~ "a> er også tilgjengelig, for å si opp postlisteabonnementer. "

#~ msgid "Mailing List Subscription"
#~ msgstr "Postliste-abonnement"

#~ msgid ""
#~ "<total_consultant> Debian consultants listed in <total_country> countries "
#~ "worldwide."
#~ msgstr "<total_consultant> Debian-konsulenter i <total_country> land vist."

#~ msgid "Willing to Relocate"
#~ msgstr "Villig å flytte"

#~ msgid "Rates:"
#~ msgstr "Takst:"

#~ msgid "Email:"
#~ msgstr "E-post:"

#~ msgid "or"
#~ msgstr "eller"

#~ msgid "URL:"
#~ msgstr "URL:"

#~ msgid "Company:"
#~ msgstr "Selskap:"

#~ msgid "Name:"
#~ msgstr "Navn:"

#~ msgid "Where:"
#~ msgstr "Hvor:"

#~ msgid "Specifications:"
#~ msgstr "Spesifikasjoner:"

#~ msgid "Architecture:"
#~ msgstr "Arkitektur:"

#~ msgid "Who:"
#~ msgstr "Hvem:"

#~ msgid "Wanted:"
#~ msgstr "Ønsket:"
