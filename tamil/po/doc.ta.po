# translation of doc.po to தமிழ்
# ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: doc\n"
"PO-Revision-Date: 2007-11-07 13:57+0530\n"
"Last-Translator: ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>\n"
"Language-Team: தமிழ் <ubuntu-l10n-tam@lists.ubuntu.com>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: ../../english/doc/books.data:31
msgid ""
"\n"
"  Debian 8 is the must-have handbook for learning Linux. Start on the\n"
"  beginners level and learn how to deploy the system with graphical\n"
"  interface and terminal.\n"
"  This book provides the basic knowledge to grow and become a 'junior'\n"
"  systems administrator. Start off with exploring the GNOME desktop\n"
"  interface and adjust it to your personal needs. Overcome your fear of\n"
"  using the Linux terminal and learn the most essential commands in\n"
"  administering Debian. Expand your knowledge of system services (systemd)\n"
"  and learn how to adapt them. Get more out of the software in Debian and\n"
"  outside of Debian. Manage your home-network with network-manager, etc.\n"
"  10 percent of the profits on this book will be donated to the Debian\n"
"  Project."
msgstr ""

#: ../../english/doc/books.data:59 ../../english/doc/books.data:163
#: ../../english/doc/books.data:216
msgid ""
"Written by two Debian developers, this free book\n"
"  started as a translation of their French best-seller known as Cahier de\n"
"  l'admin Debian (published by Eyrolles). Accessible to all, this book\n"
"  teaches the essentials to anyone who wants to become an effective and\n"
"  independent Debian GNU/Linux administrator.\n"
"  It covers all the topics that a competent Linux administrator should\n"
"  master, from the installation and the update of the system, up to the\n"
"  creation of packages and the compilation of the kernel, but also\n"
"  monitoring, backup and migration, without forgetting advanced topics\n"
"  like SELinux setup to secure services, automated installations, or\n"
"  virtualization with Xen, KVM or LXC."
msgstr ""

#: ../../english/doc/books.data:80
msgid ""
"The aim of this freely available, up-to-date, book is to get you up to\n"
"  speed with Debian (including both the current stable release and the\n"
"  current unstable distribution). It is comprehensive with basic support\n"
"  for the user who installs and maintains the system themselves (whether\n"
"  in the home, office, club, or school)."
msgstr ""

#: ../../english/doc/books.data:101
msgid ""
"The first French book about Debian is already in its fifth edition. It\n"
"  covers all aspects of the administration of Debian from the installation\n"
"  to the configuration of network services.\n"
"  Written by two Debian developers, this book can be of interest to many\n"
"  people: the beginner wishing to discover Debian, the advanced user "
"looking\n"
"  for tips to enhance his mastership of the Debian tools and the\n"
"  administrator who wants to build a reliable network with Debian."
msgstr ""

#: ../../english/doc/books.data:120
msgid ""
"The book covers topics ranging from concepts of package\n"
"  management over the available tools and how they're used to concrete "
"problems\n"
"  which may occur in real life and how they can be solved. The book is "
"written\n"
"  in German, an English translation is planned. Format: e-book (Online, "
"HTML,\n"
"  PDF, ePub, Mobi), printed book planned.\n"
msgstr ""

#: ../../english/doc/books.data:140
msgid ""
"This book teaches you how to install and configure the system and also how "
"to use Debian in a professional environment.  It shows the full potential of "
"the distribution (in its current version 8) and provides a practical manual "
"for all users who want to learn more about Debian and its range of services."
msgstr ""

#: ../../english/doc/books.data:191
msgid ""
"Written by two penetration researcher - Annihilator, Firstblood.\n"
"  This book teaches you how to build and configure Debian 8.x server system\n"
"  security hardening using Kali Linux and Debian simultaneously.\n"
"  DNS, FTP, SAMBA, DHCP, Apache2 webserver, etc.\n"
"  From the perspective that 'the origin of Kali Linux is Debian', it "
"explains\n"
"  how to enhance Debian's security by applying the method of penetration\n"
"  testing.\n"
"  This book covers various security issues like SSL cerificates, UFW "
"firewall,\n"
"  MySQL Vulnerability, commercial Symantec antivirus, including Snort\n"
"  intrusion detection system."
msgstr ""

#: ../../english/doc/books.def:38
msgid "Author:"
msgstr "இயற்றியவர்:"

#: ../../english/doc/books.def:41
msgid "email:"
msgstr "மின்னஞ்சல்:"

#: ../../english/doc/books.def:45
msgid "Available at:"
msgstr "கிடைக்கப் பெறுமிடம்:"

#: ../../english/doc/books.def:48
msgid "CD Included:"
msgstr "வட்டு உள்ளடக்கப்பட்டது:"

#: ../../english/doc/books.def:51
msgid "Publisher:"
msgstr "பதிப்பித்தவர்:"

#: ../../english/doc/manuals.defs:28
msgid "Authors:"
msgstr "இயற்றியோர்:"

#: ../../english/doc/manuals.defs:35
msgid "Editors:"
msgstr "தொகுத்தோர்:"

#: ../../english/doc/manuals.defs:42
msgid "Maintainer:"
msgstr "பராமரிப்பாளர்:"

#: ../../english/doc/manuals.defs:49
msgid "Status:"
msgstr "நிலை:"

#: ../../english/doc/manuals.defs:56
msgid "Availability:"
msgstr "கிடைக்கக்கூடியதா:"

#: ../../english/doc/manuals.defs:92
msgid "Latest version:"
msgstr "அண்மைய வெளியீடு:"

#: ../../english/doc/manuals.defs:108
msgid "(version <get-var version />)"
msgstr "(வெளியீடு <get-var version />)"

#: ../../english/doc/manuals.defs:138 ../../english/releases/arches.data:38
msgid "plain text"
msgstr "வெற்று உரை"

#: ../../english/doc/manuals.defs:154 ../../english/doc/manuals.defs:178
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/git\">Git</a> repository."
msgstr ""

#: ../../english/doc/manuals.defs:156 ../../english/doc/manuals.defs:164
#: ../../english/doc/manuals.defs:172 ../../english/doc/manuals.defs:180
msgid "Web interface: "
msgstr ""

#: ../../english/doc/manuals.defs:157 ../../english/doc/manuals.defs:165
#: ../../english/doc/manuals.defs:173 ../../english/doc/manuals.defs:181
msgid "VCS interface: "
msgstr ""

#: ../../english/doc/manuals.defs:162
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/cvs\">Cvs</a> repository."
msgstr ""

#: ../../english/doc/manuals.defs:170
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/subversion\">Subversion</a> repository."
msgstr ""

#: ../../english/doc/manuals.defs:188
msgid ""
"CVS sources working copy: set <code>CVSROOT</code>\n"
"  to <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
"  and check out the <kbd>boot-floppies/documentation</kbd> module."
msgstr ""
"சிவிஎஸ் மூலங்களின் நிகழ் நகல்: set <code>CVSROOT</code>\n"
"  to <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
"  and check out the <kbd>boot-floppies/documentation</kbd> module."

#: ../../english/doc/manuals.defs:193
msgid "CVS via web"
msgstr "இணைய வழி சிவிஎஸ்"

#: ../../english/doc/manuals.defs:197 ../../english/doc/manuals.defs:201
msgid "Debian package"
msgstr "டெபியன் பொதி"

#: ../../english/doc/manuals.defs:206 ../../english/doc/manuals.defs:210
#, fuzzy
#| msgid "Debian package"
msgid "Debian package (archived)"
msgstr "டெபியன் பொதி"

#: ../../english/releases/arches.data:36
msgid "HTML"
msgstr ""

#: ../../english/releases/arches.data:37
msgid "PDF"
msgstr ""

#, fuzzy
#~| msgid ""
#~| "Use <a href=\"cvs\">CVS</a> to download the SGML source text for <get-"
#~| "var ddp_pkg_loc />."
#~ msgid ""
#~ "Use <a href=\"cvs\">SVN</a> to download the SGML source text for <get-var "
#~ "ddp_pkg_loc />."
#~ msgstr ""
#~ "<a href=\"cvs\">சிவிஎஸ்</a> பயன்படுத்தி <get-var ddp_pkg_loc /> க்கான "
#~ "எஸ்ஜிஎம்எல் மூல் உரைகளை பதிவிறக்குக. "

#~ msgid "Language:"
#~ msgstr "மொழி:"
