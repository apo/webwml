# If this makefile is not generic enough to support a translation,
# please contact debian-www.

WMLBASE=../..
CUR_DIR=lts/security
SUBS= $(wildcard 2[0-9][0-9][0-9])

GETTEXTFILES += security.mo

include $(WMLBASE)/Make.lang

ifndef SUBLANG
INDEXPAGE  := index.$(LANGUAGE).html
DLARDF     := dla.$(LANGUAGE).rdf
DLALONGRDF := dla-long.$(LANGUAGE).rdf
DLAALLRDF := dla-all.$(LANGUAGE).rdf
DESTDLARDF     := $(HTMLDIR)/$(DLARDF)
DESTDLALONGRDF := $(HTMLDIR)/$(DLALONGRDF)
else
INDEXPAGE  := $(sort $(foreach i,$(SUBLANG),$(subst index,index.$(LANGUAGE)-$(i),index.html)))
DLARDF     := $(sort $(foreach i,$(SUBLANG),$(subst dla,dla.$(LANGUAGE)-$(i),dla.rdf)))
DLALONGRDF := $(sort $(foreach i,$(SUBLANG),$(subst dla-long,dla-long.$(LANGUAGE)-$(i),dla-long.rdf)))
DLAALLRDF := $(sort $(foreach i,$(SUBLANG),$(subst dla-all,dla-all.$(LANGUAGE)-$(i),dla-all.rdf)))
DESTDLARDF     := $(patsubst %.rdf,$(HTMLDIR)/%.rdf,$(DLARDF))
DESTDLALONGRDF := $(patsubst %.rdf,$(HTMLDIR)/%.rdf,$(DLALONGRDF))
endif


$(INDEXPAGE): index.wml $(wildcard $(CUR_YEAR)/dla-*.wml) \
  $(wildcard $(CUR_YEAR)/dla-*.wml) \
  $(wildcard $(ENGLISHSRCDIR)/lts/security/$(CUR_YEAR)/dla-*.wml) \
  $(wildcard $(ENGLISHSRCDIR)/lts/security/$(CUR_YEAR)/dla-*.data) \
  $(wildcard $(ENGLISHSRCDIR)/lts/security/$(CUR_YEAR)/dla-*.wml) \
  $(wildcard $(ENGLISHSRCDIR)/lts/security/$(CUR_YEAR)/dla-*.data) \
  $(TEMPLDIR)/release_info.wml \
  $(TEMPLDIR)/template.wml $(TEMPLDIR)/recent_list_security.wml $(GETTEXTDEP)

ifeq "$(LANGUAGE)" "zh"
	@echo -n "Processing $(<F): "
	$(shell echo $(WML) | perl -pe 's,:.zh-(..)\.html,:index.zh-$$1.html,g') \
          $(shell egrep '^-D (CUR_|CHAR)' ../.wmlrc) \
          $(<F)
	@$(GENERATE_ZH_VARIANTS) index html
else
	$(WML) $(<F)
endif


$(ENGLISHSRCDIR)/lts/security/ref-table.inc: $(ENGLISHSRCDIR)/lts/security/make-ref-table.pl $(wildcard $(ENGLISHSRCDIR)/lts/security/*/*.data) 
	perl $(ENGLISHSRCDIR)/lts/security/make-ref-table.pl -p -a >$(ENGLISHSRCDIR)/lts/security/ref-table.inc

crossreferences.$(LANGUAGE).html:: $(ENGLISHSRCDIR)/lts/security/ref-table.inc \
	$(ENGLISHDIR)/template/debian/securityreferences.wml 

clean::
	rm -f $(DLARDF) $(DLALONGRDF)
	rm -f ref-table.inc

$(DLARDF): $(ENGLISHDIR)/lts/security/dla.rdf.in \
  $(wildcard $(CUR_YEAR)/dla-*.wml) \
  $(wildcard $(ENGLISHDIR)/lts/security/$(CUR_YEAR)/dla-*.wml) \
  $(wildcard $(ENGLISHDIR)/lts/security/$(CUR_YEAR)/dla-*.data) \
  $(wildcard $(CUR_YEAR)/dla-*.wml) \
  $(wildcard $(ENGLISHDIR)/lts/security/$(CUR_YEAR)/dla-*.wml) \
  $(wildcard $(ENGLISHDIR)/lts/security/$(CUR_YEAR)/dla-*.data) \
  $(TEMPLDIR)/recent_list_security.wml $(GETTEXTDEP)
ifeq "$(LANGUAGE)" "zh"
	@echo -n "Processing $(<F): "
	$(shell echo $(WML) | perl -pe 's,:.zh-(..)\.html,:dla.zh-$$1.rdf,g') \
          $(shell egrep '^-D (CUR_|CHAR)' ../.wmlrc) \
            $(ENGLISHDIR)/lts/security/dla.rdf.in
	@$(GENERATE_ZH_VARIANTS) dla rdf
else
	$(WML) $(shell egrep '^-D (CUR_|CHAR)' ../.wmlrc) \
            $(ENGLISHDIR)/lts/security/dla.rdf.in
endif

$(DLALONGRDF): $(ENGLISHDIR)/lts/security/dla-long.rdf.in \
  $(wildcard $(CUR_YEAR)/dla-*.wml) \
  $(wildcard $(ENGLISHDIR)/lts/security/$(CUR_YEAR)/dla-*.wml) \
  $(wildcard $(ENGLISHDIR)/lts/security/$(CUR_YEAR)/dla-*.data) \
  $(wildcard $(CUR_YEAR)/dla-*.wml) \
  $(wildcard $(ENGLISHDIR)/lts/security/$(CUR_YEAR)/dla-*.wml) \
  $(wildcard $(ENGLISHDIR)/lts/security/$(CUR_YEAR)/dla-*.data) \
  $(TEMPLDIR)/recent_list_security.wml $(GETTEXTDEP)
ifeq "$(LANGUAGE)" "zh"
	@echo -n "Processing $(<F): "
	$(shell echo $(WML) | perl -pe 's,:.zh-(..)\.html,:dla-long.zh-$$1.rdf,g') \
          $(shell egrep '^-D (CUR_|CHAR)' ../.wmlrc) \
            $(ENGLISHDIR)/lts/security/dla-long.rdf.in
	@$(GENERATE_ZH_VARIANTS) dla-long rdf
else
	$(WML) $(shell egrep '^-D (CUR_|CHAR)' ../.wmlrc) \
            $(ENGLISHDIR)/lts/security/dla-long.rdf.in
endif

$(DLAALLRDF): $(ENGLISHDIR)/lts/security/dla-all.rdf.in \
  $(wildcard ????/dla-*.wml) \
  $(wildcard $(ENGLISHDIR)/lts/security/????/dla-*.wml) \
  $(wildcard $(ENGLISHDIR)/lts/security/????/dla-*.data) \
  $(TEMPLDIR)/recent_list.wml $(GETTEXTDEP)
ifeq "$(LANGUAGE)" "zh"
	@echo -n "Processing $(<F): "
	$(shell echo $(WML) | perl -pe 's,:.zh-(..)\.html,:dla-all.zh-$$1.rdf,g') \
          $(shell egrep '^-D (CUR_|CHAR)' ../.wmlrc) \
            $(ENGLISHDIR)/lts/security/dla-all.rdf.in
	@$(GENERATE_ZH_VARIANTS) dla-all rdf
else
	$(WML) $(shell egrep '^-D (CUR_|CHAR)' ../.wmlrc) \
            $(ENGLISHDIR)/lts/security/dla-all.rdf.in
endif


all:: $(DLARDF) $(DLALONGRDF)

install:: $(DESTDLARDF) $(DESTDLALONGRDF)

$(DESTDLARDF): $(HTMLDIR)/%: %
	@test -d $(HTMLDIR) || mkdir -m g+w -p $(HTMLDIR)
	install -m 664 -p $< $(HTMLDIR)
$(DESTDLALONGRDF): $(HTMLDIR)/%: %
	@test -d $(HTMLDIR) || mkdir -m g+w -p $(HTMLDIR)
	install -m 664 -p $< $(HTMLDIR)


ifeq "$(LANGUAGE)" "en"

map := $(HTMLDIR)/map-dla.txt

$(map): $(wildcard $(CUR_YEAR)/dla-*.wml)
	for i in 2*/dla-*.wml; do echo $$i $$i | sed -r 's/^.+\/(.+) /\1 /' | sed 's/\.wml//g' | sed 's/^dla-//' ;done > $(HTMLDIR)/map-dla.txt

install:: $(map)

cleandest::
	rm -f $(map)

endif
