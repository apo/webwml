# Brazilian Portuguese translation for Debian website stats.pot
# Copyright (C) 2012-2017 Software in the Public Interest, Inc.
#
# Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2012-2017.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian WebWML\n"
"PO-Revision-Date: 2017-02-28 21:49-0300\n"
"Last-Translator: Marcelo Gomes de Santana <marcelo@msantana.eng.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Estatísticas de tradução do site web do Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Existem %d páginas para traduzir."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Existem %d bytes para traduzir."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Existem %d strings para traduzir."

#: ../../stattrans.pl:278 ../../stattrans.pl:494
msgid "Wrong translation version"
msgstr "Versão errada de tradução"

#: ../../stattrans.pl:280
msgid "This translation is too out of date"
msgstr "Esta tradução está muito desatualizada"

#: ../../stattrans.pl:282
msgid "The original is newer than this translation"
msgstr "O original é mais novo do que esta tradução"

#: ../../stattrans.pl:286 ../../stattrans.pl:494
msgid "The original no longer exists"
msgstr "O original não existe mais"

#: ../../stattrans.pl:470
msgid "hit count N/A"
msgstr "contagem de acesso N/A"

#: ../../stattrans.pl:470
msgid "hits"
msgstr "acessos"

#: ../../stattrans.pl:488 ../../stattrans.pl:489
msgid "Click to fetch diffstat data"
msgstr "Clique para obter dados de diffstat"

#: ../../stattrans.pl:599 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr "Criado com <transstatslink>"

#: ../../stattrans.pl:604
msgid "Translation summary for"
msgstr "Resumo de tradução para"

#: ../../stattrans.pl:607 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Não traduzidos"

#: ../../stattrans.pl:607 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Desatualizados"

#: ../../stattrans.pl:607
msgid "Translated"
msgstr "Traduzidos"

#: ../../stattrans.pl:607 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "Atualizados"

#: ../../stattrans.pl:608 ../../stattrans.pl:609 ../../stattrans.pl:610
#: ../../stattrans.pl:611
msgid "files"
msgstr "arquivos"

#: ../../stattrans.pl:614 ../../stattrans.pl:615 ../../stattrans.pl:616
#: ../../stattrans.pl:617
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:624
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Nota: as listas das páginas são classificadas por popularidade. Passe o "
"mouse sobre o nome da página para ver o número de acessos."

#: ../../stattrans.pl:630
msgid "Outdated translations"
msgstr "Traduções desatualizadas"

#: ../../stattrans.pl:632 ../../stattrans.pl:686
msgid "File"
msgstr "Arquivo"

#: ../../stattrans.pl:634
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:636
msgid "Comment"
msgstr "Comentário"

#: ../../stattrans.pl:637
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Log"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Tradução"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Mantenedor"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Tradutor"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Data"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Páginas gerais não traduzidas"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Páginas gerais não traduzidas"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Itens de notícias não traduzidos"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Itens de notícias não traduzidos"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Páginas de consultor/usuário não traduzidas"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Páginas de consultor/usuário não traduzidas"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Páginas internacionais não traduzidas"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Páginas internacionais não traduzidas"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Páginas traduzidas (atualidadas)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Templates traduzidos (arquivos PO)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "Estatísticas de tradução de PO"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Imprecisos"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Não traduzidos"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Total"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Total:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Páginas web traduzidas"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Estatísticas de Tradução por Número de Página"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Idioma"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Traduções"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Páginas web traduzidas (por tamanho)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Estatísticas de Tradução por Tamanho de Página"

#~ msgid "Created with"
#~ msgstr "Criado com"

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "diff colorido"

#~ msgid "Colored diff"
#~ msgstr "diff colorido"

#~ msgid "Unified diff"
#~ msgstr "diff unificado"
